FROM golang:1.13-alpine as build

WORKDIR /go/src/gitlab.com/vl_sa/vl_sa

COPY . .

RUN go build -o app

FROM alpine:3.7

RUN apk add --no-cache ca-certificates

COPY --from=build /go/src/gitlab.com/vl_sa/vl_sa/app /usr/local/bin/app

COPY --from=build /go/src/gitlab.com/vl_sa/vl_sa/html/static /resources

CMD ["/usr/local/bin/app"]